using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : Photon.MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;

    public HealthBar healthBar;

    void Start()
    {
        //if (photonView.isMine)
        //{
            currentHealth = maxHealth;
            healthBar.setMaxHealth(maxHealth);
        //}
    }

    void Update()
    {
        //if (photonView.isMine)
        //{
            if (Input.GetKeyDown(KeyCode.B))
            {
                TakeDamage(20);
            }
        //}
    }

    void TakeDamage(int damage)
    {
            currentHealth -= damage;

            healthBar.setHealth(currentHealth);
    }
}
