using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraRotate : Photon.MonoBehaviour
{
    PlayerInput input;

    void Awake()
    {
        input = new PlayerInput();
    }

    void Update()
    {
        if (photonView.isMine)
        {
            input.CharacterControls.CameraRotate.performed += ctx => ctx.ReadValue<Vector2>();
        }
    }
}
