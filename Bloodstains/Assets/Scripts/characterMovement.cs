﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class characterMovement : Photon.MonoBehaviour
{
    public PhotonView photonView;
    public GameObject PlayerCamera;
    public Text PlayerNameText1;
    public Text PlayerNameText2;
    public GameObject playerHealthbar;
    public GameObject enemyHealthbar;
    //public Text PlayerNameText;

    Animator animator;
    //[SerializeField] Movement movement;

    int isWalkingHash;
    int isRunningHash;
    int isJumpingHash;
    int isCrouchingHash;
    // int isSlashingHash;
    // int isBlockingHash;
    // int isShortSlashingHash;
    // int isFlippingHash;
    // int isCastingHash;
    // int isDashingHash;

    PlayerInput input;

    Vector2 currentMovement;
    bool movementPressed;
    bool runPressed;
    bool jumpPressed;
    bool crouchPressed;
    // bool slashPressed;
    // bool blockPressed;
    // bool shortslashPressed;
    // bool flipPressed;
    // bool castPressed;
    // bool dashPressed;

    void Awake()
    {

        //HealthBar bar1 = new HealthBar();
        //bar1.

        if (photonView.isMine)
        {
            PlayerCamera.SetActive(true);
            //playerHealthbar.slider.value = 100;
            PlayerNameText1.text = PhotonNetwork.playerName;
            //PlayerNameText2.text.SetActive(false);
        }
        else
        {
            PlayerNameText2.text = photonView.owner.name;
            PlayerNameText2.color = Color.cyan;
            //PlayerNameText1.enabled(false);
        }

        input = new PlayerInput();

        input.CharacterControls.Movement.performed += ctx =>
        {
            currentMovement = ctx.ReadValue<Vector2>();
            movementPressed = currentMovement.x != 0 || currentMovement.y != 0;
        };

        input.CharacterControls.Run.performed += ctx => runPressed = ctx.ReadValueAsButton();

        input.CharacterControls.Crouch.performed += ctx => crouchPressed = ctx.ReadValueAsButton();

        // input.CharacterControls.Jump.performed += _ => jumpPressed = true;

        // //input.CharacterControls.Jump.performed += _ => ;

        // input.CharacterControls.Slash.performed += ctx => slashPressed = ctx.ReadValueAsButton();

        // input.CharacterControls.Block.performed += _ => blockPressed = true;

        // input.CharacterControls.ShortSlash.performed += ctx => animator.SetBool(isShortSlashingHash, true);

        // input.CharacterControls.Flip.performed += _ => flipPressed = true;

        // input.CharacterControls.Casting.performed += ctx => castPressed = ctx.ReadValueAsButton();

        // input.CharacterControls.Dash.performed += _ => dashPressed = true;
    }
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        isWalkingHash = Animator.StringToHash("isWalking");
        isRunningHash = Animator.StringToHash("isRunning");
        isJumpingHash = Animator.StringToHash("isJumping");
        isCrouchingHash = Animator.StringToHash("isCrouching");
        // isBlockingHash = Animator.StringToHash("isBlocking");
        // isShortSlashingHash = Animator.StringToHash("isShortSlashing");
        // isFlippingHash = Animator.StringToHash("isFlipping");
        // isCastingHash = Animator.StringToHash("isCasting");
        // isDashingHash = Animator.StringToHash("isDashing");
        // isSlashingHash = Animator.StringToHash("isSlashing");
    }

    // Update is called once per frame
    void Update()
    {

        if (photonView.isMine)
        {   
            handleMovement();
            handleRotation();
        }
        // if (currentMovement.x == 0 && currentMovement.y == 0)
        // {
        //     animator.SetBool(isWalkingHash, false);
        // }
    }
    
    void handleRotation()
    {

            if(Time.timeScale == 1f){
            Vector3 currentPosition = transform.position;

            Vector3 newPosition = new Vector3(currentMovement.x, 0, currentMovement.y);

            Vector3 positionToLookAt = currentPosition + newPosition;

            transform.LookAt(positionToLookAt);
            }

    }

    // void onjumpPressed()
    // {
    //     jump = true;
    // }

    // void Jump()
    // {
    //          animator.SetTrigger("jumpTrigger");
    // }

    void handleMovement()
    {

            bool isRunning = animator.GetBool(isRunningHash);
            bool isWalking = animator.GetBool(isWalkingHash);
            // bool isJumping = animator.GetBool(isJumpingHash);
            bool isCrouching = animator.GetBool(isCrouchingHash);
            // bool isBlocking = animator.GetBool(isBlockingHash);
            // bool isShortSlashing = animator.GetBool(isShortSlashingHash);
            // bool isFlipping= animator.GetBool(isFlippingHash);
            // bool isCasting = animator.GetBool(isCastingHash);
            // bool isDashing = animator.GetBool(isDashingHash);
            // bool isSlashing = animator.GetBool(isSlashingHash);

            input.CharacterControls.Jump.performed += _ => animator.SetTrigger("jumpTrigger");
            input.CharacterControls.Slash.performed += _ => animator.SetTrigger("slashTrigger");
            input.CharacterControls.ShortSlash.performed += _ => animator.SetTrigger("shortSlashTrigger");
            input.CharacterControls.Block.performed += _ => animator.SetTrigger("blockTrigger");
            input.CharacterControls.Flip.performed += _ => animator.SetTrigger("flipTrigger");
            input.CharacterControls.Casting.performed += _ => animator.SetTrigger("castTrigger");
            input.CharacterControls.Dash.performed += _ => animator.SetTrigger("dashTrigger");

            // if (!isDashing && dashPressed)
            // {
            //     animator.SetBool(isDashingHash, true);
            //     dashPressed = false;
            // }

            // if (isDashing && !dashPressed)
            // {
            //     animator.SetBool(isDashingHash, false);
            // }

            // if (!isCasting && castPressed)
            // {
            //     animator.SetBool(isCastingHash, true);
            // }

            // if (isCasting && !castPressed)
            // {
            //     animator.SetBool(isCastingHash, false);
            // }

            // if (!isFlipping && flipPressed)
            // {
            //     animator.SetBool(isFlippingHash, true);
            //     flipPressed = false;
            // }

            // if (isFlipping && !flipPressed)
            // {
            //     animator.SetBool(isFlippingHash, false);
            // }

            // if (!isBlocking && blockPressed && !isJumping)
            // {
            //     animator.SetBool(isBlockingHash, true);
            // }

            // if (isBlocking && !blockPressed && !isJumping)
            // {
            //     animator.SetBool(isBlockingHash, false);
            // }

            if (!isCrouching && crouchPressed)
            {
                animator.SetBool(isCrouchingHash, true);
            }

            if (isCrouching && !crouchPressed)
            {
                animator.SetBool(isCrouchingHash, false);
            }

            // if (!isJumping && slashPressed && !isSlashing && !isShortSlashing)
            // {
            //     //animator.SetBool(isSlashingHash, true);
            //     animator.SetTrigger("jumpTrigger");
            // }

            // // if (!isJumping && !slashPressed && isSlashing && !isShortSlashing)
            // // {
            // //     animator.SetBool(isSlashingHash, false);
            // // }

            // if (!isJumping && shortslashPressed && !isShortSlashing)
            // {
            //     animator.SetBool(isShortSlashingHash, true);
            // }

            // // if (!isJumping && !shortslashPressed && isShortSlashing)
            // // {
            // //     animator.SetBool(isShortSlashingHash, false);
            // // }

            // //if (!isWalking && jumpTapped && !jumpCancelled && !isJumping)
            // //{
            // //    animator.SetBool(isJumpingHash, true);
            // //}

            // //if (!isWalking && !jumpTapped && jumpCancelled && isJumping)
            // //{
            // //    animator.SetBool(isJumpingHash, false);
            // //}

            if (movementPressed && !isWalking)
            {
                animator.SetBool(isWalkingHash, true);
            }

            if (!movementPressed && isWalking)
            {
                animator.SetBool(isWalkingHash, false);
            }

            if ((movementPressed && runPressed) && !isRunning)
            {
                animator.SetBool(isRunningHash, true);
            }

            if ((!movementPressed || !runPressed) && isRunning)
            {
                animator.SetBool(isRunningHash, false);
            }
            
            // // if (jumpPressed && !isJumping)
            // // {
            // //     animator.SetBool(isJumpingHash, true);
            // // }

            // // if (!jumpPressed && isJumping)
            // // {
            // //     animator.SetBool(isJumpingHash, false);
            // // }

            // if (jumpPressed && !isJumping)
            // {   
            //         animator.SetBool(isJumpingHash, true);
            //         jumpPressed = false;
            // }

            // if (!jumpPressed && isJumping)
            // {   
            //         animator.SetBool(isJumpingHash, false);
            // }

            //if (isJumping && jumpPressed)
            //{
            //    animator.SetBool(isJumpingHash, false);
            //}
    }

    void OnEnable()
    {
        input.CharacterControls.Enable();
    }

    void OnDisable()
    {
        input.CharacterControls.Disable();
    }

    // [PunRPC]
    // private void Jump()
    // {
    //     animator.SetTrigger("jumpTrigger");
    // }
}
