# Bloodstained

A Bloodstained fantázianevű játékunk egy 3D third person view, japán stílusú szamurájos játék. A játéknak nincs különösebb története, a lényege, hogy egymással tudunk harcolni az elkészített japán stilusú mapon. A szervert Photon segítségével oldottuk meg, jelenleg max. 4 játékos tud csatlakozni. A játékhoz egyébként Solti Dániel írt saját zenét. Még van néhány hiányosság a játékban irányítás szempontjából, illetve a damage rendszert nem sikerült még megoldanunk.

Irányítás:
WASD, left click - hit, right click - block, C - crouch, E - casting, D - dash, L - slash, kameramozgatás egérrel

Csapattagok:
Solti Dániel - solti.dani7@gmail.com (irányitás, zene, animációk)
Gebhardt Bálint - gebhardtbalint1@gmail.com (map, kamera, menü design)
